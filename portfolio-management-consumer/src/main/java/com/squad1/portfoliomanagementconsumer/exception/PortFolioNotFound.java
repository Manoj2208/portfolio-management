package com.squad1.portfoliomanagementconsumer.exception;

import com.squad1.portfoliomanagementconsumer.util.ErrorConstants;

public class PortFolioNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PortFolioNotFound() {
		super(ErrorConstants.PORTFOLIO_NOTFOUND, ErrorConstants.CODE_PORTFOLIONOTFOUND);
	}

}
