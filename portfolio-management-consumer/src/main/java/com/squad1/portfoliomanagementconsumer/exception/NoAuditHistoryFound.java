package com.squad1.portfoliomanagementconsumer.exception;

import com.squad1.portfoliomanagementconsumer.util.ErrorConstants;

public class NoAuditHistoryFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoAuditHistoryFound() {
		super(ErrorConstants.NOAUDITHISTORY_FOUND, ErrorConstants.CODE_NOAUDITHISTORY);
	}

}
