package com.squad1.portfoliomanagementconsumer.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Audit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long auditId;
	private String transactionRef;
	private Long instrumentId;
	private String instrumentName;
	private Long customerId;
	private String customerName;
	@Enumerated(EnumType.STRING)
	private TradeType tradeType;
	private Integer noOfUnits;
	private Long portfolioId;
	@CreationTimestamp
	private LocalDateTime dateTime;
}
