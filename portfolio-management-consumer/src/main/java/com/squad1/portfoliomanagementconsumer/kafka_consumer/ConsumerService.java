package com.squad1.portfoliomanagementconsumer.kafka_consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squad1.portfoliomanagementconsumer.dto.AuditMessageDto;
import com.squad1.portfoliomanagementconsumer.service.impl.AuditConsumerServiceImpl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class ConsumerService {

	private final AuditConsumerServiceImpl auditConsumerServiceImpl;

	@KafkaListener(topics = "audit-queue", groupId = "group-id3")
	public void consumeAuditLog(ConsumerRecord<String, Object> audit) {
		
		log.info("Audit consumed from consumer" + audit.value());
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = null;

		try {
			node = mapper.readTree(audit.value().toString());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		AuditMessageDto auditMessageDto = AuditMessageDto.builder().customerId(node.get("customerId").asLong())
				.customerName(node.get("customerName").asText()).instrumentId(node.get("instrumentId").asLong())
				.instrumentName(node.get("instrumentName").asText()).noOfUnits(node.get("noOfUnits").asInt())
				.portfolioId(node.get("portfolioId").asLong()).tradeType(node.get("tradeType").asText())
				.transactionRef(node.get("transactionRef").asText()).build();
		auditConsumerServiceImpl.saveAudit(auditMessageDto);

	}
}
