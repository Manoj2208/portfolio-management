package com.squad1.portfoliomanagementconsumer.controller;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad1.portfoliomanagementconsumer.dto.AuditDTO;
import com.squad1.portfoliomanagementconsumer.entity.TradeType;
import com.squad1.portfoliomanagementconsumer.service.AuditService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class AuditController {

	private final AuditService auditService;

	/**
	 * Getting list of Audit table
	 * 
	 * @param portfolioId getting audit details
	 * @param tradeType   using to filter
	 * @param page        indicates page number
	 * @param size        indicates page size
	 * @return List of Audit table
	 */
	@GetMapping("/{portfolioId}")
	public ResponseEntity<Page<AuditDTO>> getAuditDetails(@PathVariable Long portfolioId,
			@RequestParam TradeType tradeType,
			@Valid @Min(value = 0) @RequestParam(required = false, defaultValue = "0") int page,
			@Min(value = 1) @RequestParam(required = false, defaultValue = "1") int size) {
		log.info("Fetching the list of Audit table");
		return ResponseEntity.status(HttpStatus.OK)
				.body(auditService.getAuditDetails(portfolioId, tradeType, page, size));
	}

}
