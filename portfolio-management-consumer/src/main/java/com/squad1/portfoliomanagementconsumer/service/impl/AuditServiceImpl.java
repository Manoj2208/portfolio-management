package com.squad1.portfoliomanagementconsumer.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.squad1.portfoliomanagementconsumer.dto.AuditDTO;
import com.squad1.portfoliomanagementconsumer.entity.TradeType;
import com.squad1.portfoliomanagementconsumer.exception.PortFolioNotFound;
import com.squad1.portfoliomanagementconsumer.repository.AuditRepository;
import com.squad1.portfoliomanagementconsumer.service.AuditService;
import com.squad1.portfoliomanagementconsumer.util.ErrorConstants;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuditServiceImpl implements AuditService {

	private final AuditRepository auditRepository;

	@Override
	public Page<AuditDTO> getAuditDetails(Long portfolioId, TradeType tradeType, int page, int size) {
		log.info("checking for valid portfolioId");
		if (!auditRepository.existsByPortfolioId(portfolioId)) {
			log.error(ErrorConstants.PORTFOLIO_NOTFOUND);
			throw new PortFolioNotFound();
		}
		Pageable pageable = PageRequest.of(page, size);
		log.info("Return list of audit table");
		return auditRepository.findByPortfolioIdAndTradeType(portfolioId, tradeType, pageable)
				.map(audit -> AuditDTO.builder().transactionRef(audit.transactionRef())
						.instrumentId(audit.instrumentId()).instrumentName(audit.instrumentName())
						.customerId(audit.customerId()).customerName(audit.customerName()).tradeType(audit.tradeType())
						.noOfUnits(audit.noOfUnits()).portfolioId(audit.portfolioId()).dateTime(audit.dateTime())
						.build());
	}
}
