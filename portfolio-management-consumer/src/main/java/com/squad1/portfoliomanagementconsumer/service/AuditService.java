package com.squad1.portfoliomanagementconsumer.service;

import org.springframework.data.domain.Page;

import com.squad1.portfoliomanagementconsumer.dto.AuditDTO;
import com.squad1.portfoliomanagementconsumer.entity.TradeType;

public interface AuditService {


     Page<AuditDTO> getAuditDetails(Long portfolioId, TradeType tradeType, int page, int size) ;

}
