package com.squad1.portfoliomanagementconsumer.service.impl;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import com.squad1.portfoliomanagementconsumer.dto.AuditMessageDto;
import com.squad1.portfoliomanagementconsumer.entity.Audit;
import com.squad1.portfoliomanagementconsumer.entity.TradeType;
import com.squad1.portfoliomanagementconsumer.repository.AuditRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuditConsumerServiceImpl {
	
	private final AuditRepository auditRepository;

	public void saveAudit(AuditMessageDto auditMessageDto) {
		Audit audit = Audit.builder().customerId(auditMessageDto.customerId()).customerName(auditMessageDto.customerName())
				.dateTime(LocalDateTime.now()).instrumentId(auditMessageDto.instrumentId())
				.instrumentName(auditMessageDto.instrumentName()).noOfUnits(auditMessageDto.noOfUnits())
				.portfolioId(auditMessageDto.portfolioId()).tradeType(TradeType.valueOf(auditMessageDto.tradeType()))
				.transactionRef(auditMessageDto.transactionRef()).build();
		log.info("audit persisted to db");
		auditRepository.save(audit);
	}

}
