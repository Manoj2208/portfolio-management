package com.squad1.portfoliomanagementconsumer.util;

public interface ErrorConstants {

	String CODE_PORTFOLIONOTFOUND = "ERROR01";
	String PORTFOLIO_NOTFOUND = "Portfolio Not Found";

	String CODE_NOAUDITHISTORY = "ERROR04";
	String NOAUDITHISTORY_FOUND = "No Audit History";

}
