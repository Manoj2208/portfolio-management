package com.squad1.portfoliomanagementconsumer.dto;

import java.time.LocalDateTime;

import com.squad1.portfoliomanagementconsumer.entity.TradeType;

import lombok.Builder;

@Builder
public record AuditDTO(Long portfolioId, String transactionRef, Long instrumentId, String instrumentName,
		Long customerId, String customerName, TradeType tradeType, Integer noOfUnits,

		LocalDateTime dateTime) {

}
