package com.squad1.portfoliomanagementconsumer.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message,String httpStatus) {

}
