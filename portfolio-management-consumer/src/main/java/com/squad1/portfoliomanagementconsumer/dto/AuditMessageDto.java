package com.squad1.portfoliomanagementconsumer.dto;

import lombok.Builder;

@Builder
public record AuditMessageDto(String transactionRef, Long instrumentId, String instrumentName, Long customerId,
		String customerName, String tradeType, Integer noOfUnits, Long portfolioId) {

}
