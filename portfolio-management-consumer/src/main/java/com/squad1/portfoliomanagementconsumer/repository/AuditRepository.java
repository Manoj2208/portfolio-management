package com.squad1.portfoliomanagementconsumer.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.portfoliomanagementconsumer.dto.AuditDTO;
import com.squad1.portfoliomanagementconsumer.entity.Audit;
import com.squad1.portfoliomanagementconsumer.entity.TradeType;

public interface AuditRepository extends JpaRepository<Audit, Long> {


	Page<AuditDTO> findByPortfolioIdAndTradeType(Long portfolioId, TradeType tradeType, Pageable pageable);

	boolean existsByPortfolioId(Long portfolioId);


}
