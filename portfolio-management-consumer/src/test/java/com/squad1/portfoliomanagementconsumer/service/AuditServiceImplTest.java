package com.squad1.portfoliomanagementconsumer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.squad1.portfoliomanagementconsumer.dto.AuditDTO;
import com.squad1.portfoliomanagementconsumer.entity.Audit;
import com.squad1.portfoliomanagementconsumer.entity.TradeType;
import com.squad1.portfoliomanagementconsumer.exception.PortFolioNotFound;
import com.squad1.portfoliomanagementconsumer.repository.AuditRepository;
import com.squad1.portfoliomanagementconsumer.service.impl.AuditServiceImpl;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@ExtendWith(MockitoExtension.class)
class AuditServiceImplTest {
	@Mock
	private AuditRepository auditRepository;

	@InjectMocks
	private AuditServiceImpl auditService;

	@Test
	void getAuditDetailsSuccess() {
		Long portfolioId = 1L;
		TradeType tradeType = TradeType.BUY;
		int page = 0;
		int size = 10;
		List<AuditDTO> dtoList = new ArrayList<>();
		dtoList.add(new AuditDTO(1l, "ABC123", 1l, "stock", 1l, "niharika vere", tradeType, 10,
				LocalDateTime.now().minusDays(2)));
		List<Audit> auditList = new ArrayList<>();
		Audit audit = new Audit();
		audit.setTransactionRef("ABC123");
		audit.setInstrumentId(100L);
		audit.setInstrumentName("Stocks");
		audit.setCustomerId(123L);
		audit.setCustomerName("Niharika");
		audit.setTradeType(TradeType.BUY);
		audit.setNoOfUnits(100);
		audit.setPortfolioId(1L);
		auditList.add(audit);

		when(auditRepository.existsByPortfolioId(Mockito.anyLong())).thenReturn(true);
		Pageable pageable = Mockito.mock(Pageable.class);
		Page<AuditDTO> mockPage = new PageImpl<>(dtoList, pageable, dtoList.size());
		when(auditRepository.findByPortfolioIdAndTradeType(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenReturn(mockPage);

		Page<AuditDTO> result = auditService.getAuditDetails(portfolioId, tradeType, page, size);

		assertEquals(1, result.getTotalElements());
		assertEquals(audit.getTransactionRef(), result.getContent().get(0).transactionRef());
	}

	@Test
	void getAuditDetailsPortfolioNotFound() {
		Long portfolioId = 1L;
		TradeType tradeType = TradeType.BUY;
		int page = 0;
		int size = 10;
		when(auditRepository.existsByPortfolioId(portfolioId)).thenReturn(false);
		assertThrows(PortFolioNotFound.class, () -> auditService.getAuditDetails(portfolioId, tradeType, page, size));
	}

	@Test
	void getAuditDetailsNoRecordsFound() {
		Long portfolioId = 1L;
		TradeType tradeType = TradeType.BUY;
		int page = 0;
		int size = 10;

		when(auditRepository.existsByPortfolioId(portfolioId)).thenReturn(true);
		when(auditRepository.findByPortfolioIdAndTradeType(portfolioId, tradeType, PageRequest.of(page, size)))
				.thenReturn(Page.empty());

		Page<AuditDTO> result = auditService.getAuditDetails(portfolioId, tradeType, page, size);
		assertEquals(0, result.getTotalElements());
	}

}
