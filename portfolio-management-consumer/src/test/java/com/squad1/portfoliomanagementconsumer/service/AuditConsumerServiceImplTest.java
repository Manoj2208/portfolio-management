package com.squad1.portfoliomanagementconsumer.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad1.portfoliomanagementconsumer.dto.AuditMessageDto;
import com.squad1.portfoliomanagementconsumer.entity.Audit;
import com.squad1.portfoliomanagementconsumer.entity.TradeType;
import com.squad1.portfoliomanagementconsumer.repository.AuditRepository;
import com.squad1.portfoliomanagementconsumer.service.impl.AuditConsumerServiceImpl;

@ExtendWith(SpringExtension.class)
class AuditConsumerServiceImplTest {
	@Mock
	private AuditRepository auditRepository;

	@InjectMocks
	private AuditConsumerServiceImpl auditConsumerServiceImpl;

	@Test
	void test() {
		AuditMessageDto auditMessageDto = AuditMessageDto.builder().customerId(1l).customerName("Manoj")
				.instrumentId(1l).instrumentName("lic").noOfUnits(10).portfolioId(2l)
				.tradeType(TradeType.BUY.toString()).transactionRef("23rds").build();
		Audit audit = Audit.builder().customerId(auditMessageDto.customerId())
				.customerName(auditMessageDto.customerName()).dateTime(LocalDateTime.now())
				.instrumentId(auditMessageDto.instrumentId()).instrumentName(auditMessageDto.instrumentName())
				.noOfUnits(auditMessageDto.noOfUnits()).portfolioId(auditMessageDto.portfolioId())
				.tradeType(TradeType.valueOf(auditMessageDto.tradeType()))
				.transactionRef(auditMessageDto.transactionRef()).build();
		Mockito.when(auditRepository.save(audit)).thenReturn(audit);
		doNothing().when(auditConsumerServiceImpl).saveAudit(auditMessageDto);
		assertTrue(true);
	}
}
