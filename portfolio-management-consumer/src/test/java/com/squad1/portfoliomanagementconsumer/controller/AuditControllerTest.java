package com.squad1.portfoliomanagementconsumer.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.squad1.portfoliomanagementconsumer.dto.AuditDTO;
import com.squad1.portfoliomanagementconsumer.entity.TradeType;
import com.squad1.portfoliomanagementconsumer.exception.PortFolioNotFound;
import com.squad1.portfoliomanagementconsumer.service.AuditService;

import lombok.RequiredArgsConstructor;
@RequiredArgsConstructor
@ExtendWith(MockitoExtension.class)
 class AuditControllerTest {
	 @Mock
	    private AuditService auditService;

	    @InjectMocks
	    private AuditController auditController;

	   

	    @Test
	    void getAuditDetailsWhenValidInput() {
	        Long portfolioId = 1L;
	        TradeType tradeType = TradeType.BUY;
	        int page = 0;
	        int size = 10;
	        Page<AuditDTO> auditDTOPage = mock(Page.class);
	        when(auditService.getAuditDetails(portfolioId, tradeType, page, size)).thenReturn(auditDTOPage);

	        ResponseEntity<Page<AuditDTO>> responseEntity = auditController.getAuditDetails(portfolioId, tradeType, page, size);

	        assert responseEntity.getStatusCode() == HttpStatus.OK;
	        assert responseEntity.getBody() == auditDTOPage;

	        verify(auditService).getAuditDetails(portfolioId, tradeType, page, size);
	    }

	   
	    
}
