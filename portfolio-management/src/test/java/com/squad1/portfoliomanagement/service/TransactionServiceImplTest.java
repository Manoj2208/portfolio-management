package com.squad1.portfoliomanagement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad1.portfoliomanagement.dto.ApiResponse;
import com.squad1.portfoliomanagement.dto.AuditDto;
import com.squad1.portfoliomanagement.dto.TransactionDto;
import com.squad1.portfoliomanagement.entity.Instrument;
import com.squad1.portfoliomanagement.entity.InstrumentType;
import com.squad1.portfoliomanagement.entity.InvestmentStrategy;
import com.squad1.portfoliomanagement.entity.Portfolio;
import com.squad1.portfoliomanagement.entity.Position;
import com.squad1.portfoliomanagement.entity.TradeType;
import com.squad1.portfoliomanagement.exceptions.InstrumentNotFound;
import com.squad1.portfoliomanagement.exceptions.PortFolioNotFound;
import com.squad1.portfoliomanagement.exceptions.UnitsNotAvailable;
import com.squad1.portfoliomanagement.kafka_producer.ProducerService;
import com.squad1.portfoliomanagement.repository.InstrumentRepository;
import com.squad1.portfoliomanagement.repository.PortfolioRepository;
import com.squad1.portfoliomanagement.repository.PositionRepository;
import com.squad1.portfoliomanagement.service.impl.TransactionServiceImpl;
import com.squad1.portfoliomanagement.util.SuccessResponse;

@ExtendWith(SpringExtension.class)
class TransactionServiceImplTest {
	@Mock
	private PortfolioRepository portfolioRepository;
	@Mock
	private InstrumentRepository instrumentRepository;
	@Mock
	private PositionRepository positionRepository;
	@Mock
	private ProducerService producerService;

	@InjectMocks
	private TransactionServiceImpl transactionServiceImpl;

	TransactionDto transactionDto = TransactionDto.builder().instrumentId(1l).noOfUnits(10).portfolioId(1l).build();
	Portfolio portfolio = Portfolio.builder().currentPerformance(23.4).customerId(1l).customerName("Manoj")
			.investmentStrategy(InvestmentStrategy.SAFE).portfolioId(1l).portfolioNumber("folio1")
			.portfolioValue(BigDecimal.valueOf(23456.6)).build();
	Instrument instrument = Instrument.builder().instrumentId(1l).instrumentName("lic bond")
			.instrumentType(InstrumentType.BOND).instrumentValue(BigDecimal.valueOf(2500.0)).units(25).build();
	Position position = Position.builder().amountSpent(BigDecimal.valueOf(25000.0)).dateTime(LocalDateTime.now())
			.instrument(instrument).noOfUnits(10).portfolio(portfolio).positionId(1l).build();
	AuditDto auditDto = AuditDto.builder().customerId(portfolio.getCustomerId())
			.customerName(portfolio.getCustomerName()).instrumentId(instrument.getInstrumentId())
			.instrumentName(instrument.getInstrumentName()).noOfUnits(transactionDto.noOfUnits())
			.portfolioId(portfolio.getPortfolioId()).tradeType(TradeType.BUY.toString())
			.transactionRef(UUID.randomUUID().toString()).build();

	@Test
	void testTransactionSuccess() {
		Mockito.when(portfolioRepository.findById(1l)).thenReturn(Optional.of(portfolio));
		Mockito.when(instrumentRepository.findById(1l)).thenReturn(Optional.of(instrument));
		Mockito.when(positionRepository.findByPortfolioAndInstrument(portfolio, instrument))
				.thenReturn(Optional.empty());
		Mockito.when(positionRepository.save(position)).thenReturn(position);
		Mockito.when(portfolioRepository.save(portfolio)).thenReturn(portfolio);
		Mockito.when(instrumentRepository.save(instrument)).thenReturn(instrument);
		doNothing().when(producerService).send(auditDto);
		ApiResponse apiResponse = transactionServiceImpl.transaction(TradeType.BUY, transactionDto);
		assertNotNull(apiResponse);
		assertEquals(SuccessResponse.SUCCESS_CODE2, apiResponse.httpStatus());
	}

	@Test
	void testPortfolioNotFound() {
		Mockito.when(portfolioRepository.findById(1l)).thenReturn(Optional.empty());
		assertThrows(PortFolioNotFound.class, () -> transactionServiceImpl.transaction(TradeType.BUY, transactionDto));
	}

	@Test
	void testInstrumentNotFound() {
		Mockito.when(portfolioRepository.findById(1l)).thenReturn(Optional.of(portfolio));
		Mockito.when(instrumentRepository.findById(1l)).thenReturn(Optional.empty());
		assertThrows(InstrumentNotFound.class, () -> transactionServiceImpl.transaction(TradeType.BUY, transactionDto));
	}

	@Test
	void testBuyMoreUnits() {
		instrument.setUnits(3);
		Mockito.when(portfolioRepository.findById(1l)).thenReturn(Optional.of(portfolio));
		Mockito.when(instrumentRepository.findById(1l)).thenReturn(Optional.of(instrument));
		assertThrows(UnitsNotAvailable.class, () -> transactionServiceImpl.transaction(TradeType.BUY, transactionDto));
	}

	@Test
	void testSellMoreUnits() {
		Mockito.when(portfolioRepository.findById(1l)).thenReturn(Optional.of(portfolio));
		Mockito.when(instrumentRepository.findById(1l)).thenReturn(Optional.of(instrument));
		Mockito.when(positionRepository.findByPortfolioAndInstrument(portfolio, instrument))
				.thenReturn(Optional.of(position));
		TransactionDto transactionDto = TransactionDto.builder().instrumentId(1l).noOfUnits(20).portfolioId(1l).build();
		assertThrows(UnitsNotAvailable.class, () -> transactionServiceImpl.transaction(TradeType.SELL, transactionDto));
	}

	@Test
	void testCaseBuy2() {
		Mockito.when(portfolioRepository.findById(1l)).thenReturn(Optional.of(portfolio));
		Mockito.when(instrumentRepository.findById(1l)).thenReturn(Optional.of(instrument));
		Mockito.when(positionRepository.findByPortfolioAndInstrument(portfolio, instrument))
				.thenReturn(Optional.of(position));
		Mockito.when(positionRepository.save(position)).thenReturn(position);
		Mockito.when(portfolioRepository.save(portfolio)).thenReturn(portfolio);
		Mockito.when(instrumentRepository.save(instrument)).thenReturn(instrument);
		doNothing().when(producerService).send(auditDto);
		ApiResponse apiResponse = transactionServiceImpl.transaction(TradeType.BUY, transactionDto);
		assertNotNull(apiResponse);
		assertEquals(SuccessResponse.SUCCESS_CODE2, apiResponse.httpStatus());
	}

	@Test
	void testCaseSell() {
		TransactionDto transactionDto = TransactionDto.builder().instrumentId(1l).noOfUnits(2).portfolioId(1l).build();
		Mockito.when(portfolioRepository.findById(1l)).thenReturn(Optional.of(portfolio));
		Mockito.when(instrumentRepository.findById(1l)).thenReturn(Optional.of(instrument));
		Mockito.when(positionRepository.findByPortfolioAndInstrument(portfolio, instrument))
				.thenReturn(Optional.of(position));
		Mockito.when(positionRepository.save(position)).thenReturn(position);
		Mockito.when(portfolioRepository.save(portfolio)).thenReturn(portfolio);
		Mockito.when(instrumentRepository.save(instrument)).thenReturn(instrument);
		doNothing().when(producerService).send(auditDto);
		ApiResponse apiResponse = transactionServiceImpl.transaction(TradeType.SELL, transactionDto);
		assertNotNull(apiResponse);
		assertEquals(SuccessResponse.SUCCESS_CODE2, apiResponse.httpStatus());

	}

}
