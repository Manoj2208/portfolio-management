package com.squad1.portfoliomanagement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.squad1.portfoliomanagement.dto.PortfolioResponse;
import com.squad1.portfoliomanagement.entity.Portfolio;
import com.squad1.portfoliomanagement.exceptions.PortFolioNotFound;
import com.squad1.portfoliomanagement.repository.PortfolioRepository;
import com.squad1.portfoliomanagement.service.impl.PortfolioViewServiceImpl;

@ExtendWith(MockitoExtension.class)
 class PortfolioViewServiceImplTest {
	
	@Mock
	private PortfolioRepository repository;
	
	@InjectMocks
	private PortfolioViewServiceImpl impl;
	
	@Test
	void potfolioNotFound() {
	Mockito.when(repository.findById(2l)).thenReturn(Optional.empty());
	assertThrows(PortFolioNotFound.class, ()-> impl.getByCustomerId(2l));
	}
	
	
	@Test
     void testGetByCustomerIdExistingPortfolio() {
      
        PortfolioRepository portfolioRepository = mock(PortfolioRepository.class);
        PortfolioViewServiceImpl portfolioService = new PortfolioViewServiceImpl(portfolioRepository);
        Portfolio portfolio = new Portfolio();
        portfolio.setCustomerName("yamini");
        Mockito.when(portfolioRepository.findById(1L)).thenReturn(Optional.of(portfolio));
         PortfolioResponse response = portfolioService.getByCustomerId(1L);
 
       
        assertEquals("yamini", response.portfolioDto().customerName());
    }
 
    @Test
     void testGetByCustomerIdNonExistingPortfolio() {
      
        PortfolioRepository portfolioRepository = mock(PortfolioRepository.class);
        PortfolioViewServiceImpl portfolioService = new PortfolioViewServiceImpl(portfolioRepository);
        Mockito.when(portfolioRepository.findById(2L)).thenReturn(Optional.empty());
 
     
        assertThrows(PortFolioNotFound.class, () -> portfolioService.getByCustomerId(2L));
    }
    
	
}
