package com.squad1.portfoliomanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad1.portfoliomanagement.controller.PortfolioViewController;
import com.squad1.portfoliomanagement.dto.ApiResponse;
import com.squad1.portfoliomanagement.dto.PortfolioDto;
import com.squad1.portfoliomanagement.dto.PortfolioResponse;
import com.squad1.portfoliomanagement.entity.InvestmentStrategy;
import com.squad1.portfoliomanagement.service.PortfolioViewService;


@ExtendWith(SpringExtension.class)
 class PortfolioViewControllerTest {
	@InjectMocks
	private PortfolioViewController portfolioViewController;

	@Mock
	private PortfolioViewService portfolioViewService;
	@Test
	void getportfolios() {
		ApiResponse api =new ApiResponse("201", "sucess");
		PortfolioDto portfolio=new PortfolioDto("asdf","2345as",BigDecimal.valueOf(1234.0),2345d,InvestmentStrategy.RISKY);
		PortfolioResponse response=new PortfolioResponse(api,portfolio);
		Mockito.when(portfolioViewService.getByCustomerId(1l)).thenReturn(response);
		ResponseEntity<PortfolioResponse> byCustomerId = portfolioViewController.getByCustomerId(1l);
		assertEquals(HttpStatus.OK, byCustomerId.getStatusCode());
	}
}
