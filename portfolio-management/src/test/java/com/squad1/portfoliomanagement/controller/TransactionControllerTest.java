package com.squad1.portfoliomanagement.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad1.portfoliomanagement.dto.ApiResponse;
import com.squad1.portfoliomanagement.dto.TransactionDto;
import com.squad1.portfoliomanagement.entity.TradeType;
import com.squad1.portfoliomanagement.service.TransactionService;
import com.squad1.portfoliomanagement.util.SuccessResponse;

@ExtendWith(SpringExtension.class)
class TransactionControllerTest {

	@Mock
	private TransactionService transactionService;

	@InjectMocks
	private TransactionController transactionController;

	@Test
	void testTransactionController() {
		TransactionDto transactionDto = TransactionDto.builder().instrumentId(1l).noOfUnits(10).portfolioId(1l).build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(SuccessResponse.SUCCESS_CODE2)
				.message(SuccessResponse.SUCCESS_MESSAGE2).build();
		Mockito.when(transactionService.transaction(TradeType.BUY, transactionDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = transactionController.transaction(TradeType.BUY, transactionDto);
		assertNotNull(responseEntity);
	}
}
