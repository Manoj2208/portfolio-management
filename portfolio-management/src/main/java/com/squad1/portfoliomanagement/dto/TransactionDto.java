package com.squad1.portfoliomanagement.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Builder;

@Builder
public record TransactionDto(Long portfolioId,Long instrumentId, @Min(value = 1,message = "minimum no of units required 1") @Max(value = 3,message="maximum limit for units is 3") Integer noOfUnits) {

}
