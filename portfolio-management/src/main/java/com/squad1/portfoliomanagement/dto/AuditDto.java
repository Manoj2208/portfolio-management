package com.squad1.portfoliomanagement.dto;

import lombok.Builder;

@Builder
public record AuditDto(String transactionRef, Long instrumentId, String instrumentName, Long customerId,
		String customerName, String tradeType, Integer noOfUnits, Long portfolioId) {

}
