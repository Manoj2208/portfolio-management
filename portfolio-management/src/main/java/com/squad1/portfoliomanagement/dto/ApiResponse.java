package com.squad1.portfoliomanagement.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message,String httpStatus) {

}
