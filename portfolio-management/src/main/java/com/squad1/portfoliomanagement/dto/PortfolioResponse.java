package com.squad1.portfoliomanagement.dto;

import lombok.Builder;

@Builder
public record PortfolioResponse(ApiResponse apiResponse,PortfolioDto portfolioDto) {

}
