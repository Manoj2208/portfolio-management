package com.squad1.portfoliomanagement.dto;

import java.math.BigDecimal;

import com.squad1.portfoliomanagement.entity.InvestmentStrategy;

import lombok.Builder;
@Builder
public record PortfolioDto (String customerName,String portfolioNumber,BigDecimal portfolioValue
		,Double currentPerformance,InvestmentStrategy investmentStrategy){

}
