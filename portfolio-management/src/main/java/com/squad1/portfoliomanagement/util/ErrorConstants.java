package com.squad1.portfoliomanagement.util;

public interface ErrorConstants {

	String CODE_PORTFOLIONOTFOUND = "ERROR01";
	String PORTFOLIO_NOTFOUND = "Portfolio Not Found";

	String CODE_INSTRUMENTNOTFOUND = "ERROR02";
	String INSTRUMENT_NOTFOUND = "Instrument not found";

	String CODE_UNITSNOTAVAILABLE = "ERROR03";
	String UNITS_NOTAVAILABLE = "Try for less units due to insufficient available units for transaction";


}
