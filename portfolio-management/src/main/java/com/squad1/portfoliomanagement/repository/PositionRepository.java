package com.squad1.portfoliomanagement.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.portfoliomanagement.entity.Instrument;
import com.squad1.portfoliomanagement.entity.Portfolio;
import com.squad1.portfoliomanagement.entity.Position;

public interface PositionRepository extends JpaRepository<Position, Long> {
	
	Optional<Position> findByPortfolioAndInstrument(Portfolio portfolio, Instrument instrument);
}
