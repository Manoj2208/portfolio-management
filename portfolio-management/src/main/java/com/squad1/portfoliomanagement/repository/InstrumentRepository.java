package com.squad1.portfoliomanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.portfoliomanagement.entity.Instrument;

public interface InstrumentRepository extends JpaRepository<Instrument, Long> {

}
