package com.squad1.portfoliomanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.portfoliomanagement.entity.Portfolio;

public interface PortfolioRepository extends JpaRepository<Portfolio, Long>{

      
}
