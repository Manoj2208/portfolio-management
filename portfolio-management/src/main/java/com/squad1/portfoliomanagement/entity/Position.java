package com.squad1.portfoliomanagement.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Position {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long positionId;
	@ManyToOne
	private Portfolio portfolio;
	@ManyToOne
	private Instrument instrument;
	private Integer noOfUnits;
	private BigDecimal amountSpent;
	private LocalDateTime dateTime;
}
