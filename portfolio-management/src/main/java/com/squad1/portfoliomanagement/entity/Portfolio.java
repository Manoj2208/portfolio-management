package com.squad1.portfoliomanagement.entity;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Portfolio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long portfolioId;
	private Long customerId;
	private String customerName;
	private String portfolioNumber;
	private BigDecimal portfolioValue;
	private Double currentPerformance;
	@Enumerated(EnumType.STRING)
	private InvestmentStrategy investmentStrategy;
}
