package com.squad1.portfoliomanagement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.squad1.portfoliomanagement.dto.PortfolioResponse;
import com.squad1.portfoliomanagement.service.PortfolioViewService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/portfolios/{portfolioId}")
public class PortfolioViewController {
	private final PortfolioViewService portfolioViewService;
	/**
	 * finding portfoliosummary by using custometId
	 * @param customerId to get portfoliodeails
	 * @return portfoliodeails
	 */
	@GetMapping
	public ResponseEntity<PortfolioResponse> getByCustomerId(@PathVariable Long portfolioId) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(portfolioViewService.getByCustomerId(portfolioId));
	}
}
