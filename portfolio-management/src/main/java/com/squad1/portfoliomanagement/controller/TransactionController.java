package com.squad1.portfoliomanagement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad1.portfoliomanagement.dto.ApiResponse;
import com.squad1.portfoliomanagement.dto.TransactionDto;
import com.squad1.portfoliomanagement.entity.TradeType;
import com.squad1.portfoliomanagement.service.TransactionService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class TransactionController {
	
	private final TransactionService transactionService;

	/**
	 * 
	 * @param tradeType      Buy/Sell the Instrument as transaction
	 * @param transactionDto contains portfolioId and instrumentId for transaction
	 * @return acknowledgement after the transaction as a response with status
	 */

	@PostMapping("/positions")
	public ResponseEntity<ApiResponse> transaction(@RequestParam TradeType tradeType,
			@Valid @RequestBody TransactionDto transactionDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(transactionService.transaction(tradeType, transactionDto));

	}
}
