package com.squad1.portfoliomanagement.exceptions;

public class PortFolioNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PortFolioNotFound() {
		super(com.squad1.portfoliomanagement.util.ErrorConstants.PORTFOLIO_NOTFOUND,
				com.squad1.portfoliomanagement.util.ErrorConstants.CODE_PORTFOLIONOTFOUND);
	}

}
