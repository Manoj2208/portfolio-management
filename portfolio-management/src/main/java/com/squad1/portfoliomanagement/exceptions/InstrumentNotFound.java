package com.squad1.portfoliomanagement.exceptions;

public class InstrumentNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InstrumentNotFound() {
		super(com.squad1.portfoliomanagement.util.ErrorConstants.INSTRUMENT_NOTFOUND,
				com.squad1.portfoliomanagement.util.ErrorConstants.CODE_INSTRUMENTNOTFOUND);
	}

}
