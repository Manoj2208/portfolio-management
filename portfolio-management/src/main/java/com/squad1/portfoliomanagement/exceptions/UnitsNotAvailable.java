package com.squad1.portfoliomanagement.exceptions;

import com.squad1.portfoliomanagement.util.ErrorConstants;

public class UnitsNotAvailable extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnitsNotAvailable() {
		super(ErrorConstants.UNITS_NOTAVAILABLE, ErrorConstants.CODE_UNITSNOTAVAILABLE);
	}

}
