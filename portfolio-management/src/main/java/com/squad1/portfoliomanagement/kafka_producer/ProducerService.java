package com.squad1.portfoliomanagement.kafka_producer;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import com.squad1.portfoliomanagement.dto.AuditDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ProducerService {
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(AuditDto auditDto) {

		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, AuditDto> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),
				new JsonSerializer());

		log.info("Message Sent: " + auditDto);
		ProducerRecord<String, AuditDto> auditRecord = new ProducerRecord<>("audit-queue", auditDto);
		log.info("" + auditRecord);
		log.info("" + kafkaProducer.send(auditRecord));
	}
}
