package com.squad1.portfoliomanagement.service;

import com.squad1.portfoliomanagement.dto.PortfolioResponse;

public interface PortfolioViewService {

	PortfolioResponse getByCustomerId(Long portfolioId);

	

}
