package com.squad1.portfoliomanagement.service.impl;



import java.util.Optional;

import org.springframework.stereotype.Service;

import com.squad1.portfoliomanagement.dto.ApiResponse;
import com.squad1.portfoliomanagement.dto.PortfolioDto;
import com.squad1.portfoliomanagement.dto.PortfolioResponse;
import com.squad1.portfoliomanagement.entity.Portfolio;
import com.squad1.portfoliomanagement.exceptions.PortFolioNotFound;
import com.squad1.portfoliomanagement.repository.PortfolioRepository;
import com.squad1.portfoliomanagement.service.PortfolioViewService;
import com.squad1.portfoliomanagement.util.SuccessResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class PortfolioViewServiceImpl implements PortfolioViewService {

	private final PortfolioRepository portfolioRepository;

	@Override
	public PortfolioResponse getByCustomerId(Long portfolioId) {
		Optional<Portfolio> port = portfolioRepository.findById(portfolioId);
		
		if (port.isEmpty())
		{
			log.error("No Portfoliodetails are found for Portfolio ID: {}", portfolioId);
			throw new PortFolioNotFound();
		}
		Portfolio portfolio2 = port.get();

		ApiResponse api = ApiResponse.builder().httpStatus(SuccessResponse.SUCCESS_CODE1)
				.message(SuccessResponse.SUCCESS_MESSAGE1).build();

		PortfolioDto portfolioDto = PortfolioDto.builder().customerName(portfolio2.getCustomerName())
				.currentPerformance(portfolio2.getCurrentPerformance())
				.investmentStrategy(portfolio2.getInvestmentStrategy()).portfolioNumber(portfolio2.getPortfolioNumber())
				.portfolioValue(portfolio2.getPortfolioValue()).build();
		return PortfolioResponse.builder().apiResponse(api).portfolioDto(portfolioDto).build();
	}

}
