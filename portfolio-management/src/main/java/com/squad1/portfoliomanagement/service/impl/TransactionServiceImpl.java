package com.squad1.portfoliomanagement.service.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.squad1.portfoliomanagement.dto.ApiResponse;
import com.squad1.portfoliomanagement.dto.AuditDto;
import com.squad1.portfoliomanagement.dto.TransactionDto;
import com.squad1.portfoliomanagement.entity.Instrument;
import com.squad1.portfoliomanagement.entity.Portfolio;
import com.squad1.portfoliomanagement.entity.Position;
import com.squad1.portfoliomanagement.entity.TradeType;
import com.squad1.portfoliomanagement.exceptions.InstrumentNotFound;
import com.squad1.portfoliomanagement.exceptions.PortFolioNotFound;
import com.squad1.portfoliomanagement.exceptions.UnitsNotAvailable;
import com.squad1.portfoliomanagement.kafka_producer.ProducerService;
import com.squad1.portfoliomanagement.repository.InstrumentRepository;
import com.squad1.portfoliomanagement.repository.PortfolioRepository;
import com.squad1.portfoliomanagement.repository.PositionRepository;
import com.squad1.portfoliomanagement.service.TransactionService;
import com.squad1.portfoliomanagement.util.SuccessResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService {

	private final PortfolioRepository portfolioRepository;
	private final InstrumentRepository instrumentRepository;
	private final PositionRepository positionRepository;
	private final ProducerService producerService;

	@Override
	@Transactional
	public ApiResponse transaction(TradeType tradeType, TransactionDto transactionDto) {

		Portfolio portfolio = portfolioRepository.findById(transactionDto.portfolioId()).orElseThrow(() -> {
			log.error("Invalid portfolioId:" + transactionDto.portfolioId());
			throw new PortFolioNotFound();
		});

		Instrument instrument = instrumentRepository.findById(transactionDto.instrumentId()).orElseThrow(() -> {
			log.error("Invalid instrumentId:" + transactionDto.instrumentId());
			throw new InstrumentNotFound();
		});

		if ((transactionDto.noOfUnits() >= (instrument.getUnits())) && (tradeType.equals(TradeType.BUY))) {
			log.error("Can't buy requesting quantity is more than available quantity:" + instrument.getUnits());
			throw new UnitsNotAvailable();
		}

		Optional<Position> position = positionRepository.findByPortfolioAndInstrument(portfolio, instrument);

		if ((position.isPresent()) && (tradeType.equals(TradeType.SELL))
				&& ((position.get().getNoOfUnits()) <= (transactionDto.noOfUnits()))) {
			log.error("Can't sell units due to unavailable Quantity:" + instrument.getUnits());
			throw new UnitsNotAvailable();

		}

		BigDecimal totalValue = instrument.getInstrumentValue()
				.multiply(BigDecimal.valueOf(transactionDto.noOfUnits()));
		log.info("" + totalValue);

		switch (tradeType) {

		case BUY: {
			log.info("transaction type:" + tradeType);
			instrument.setUnits(instrument.getUnits() - transactionDto.noOfUnits());
			portfolio.setPortfolioValue(portfolio.getPortfolioValue().add(totalValue));
			if (position.isEmpty()) {
				Position purschasePosition = Position.builder().amountSpent(totalValue).instrument(instrument)
						.noOfUnits(transactionDto.noOfUnits()).portfolio(portfolio).dateTime(LocalDateTime.now())
						.build();
				log.info("new transaction for the portfolio:" + purschasePosition);
				positionRepository.save(purschasePosition);

			} else if (position.isPresent()) {
				log.info("updating existing transaction for the portfolio");
				position.get().setAmountSpent(position.get().getAmountSpent().add(totalValue));
				position.get().setNoOfUnits(position.get().getNoOfUnits() + transactionDto.noOfUnits());
				position.get().setDateTime(LocalDateTime.now());
			}
		}
			break;

		case SELL: {
			log.info("transaction type:" + tradeType);
			if (position.isPresent()) {
				instrument.setUnits(instrument.getUnits() + transactionDto.noOfUnits());
				portfolio.setPortfolioValue(portfolio.getPortfolioValue().subtract(totalValue));
				position.get().setNoOfUnits(position.get().getNoOfUnits() - transactionDto.noOfUnits());
				position.get().setAmountSpent(position.get().getAmountSpent().subtract(totalValue));
			}

		}
			break;

		default:
			break;
		}

		log.info("portfolio updated");
		portfolioRepository.save(portfolio);

		log.info("instrument updated");
		instrumentRepository.save(instrument);

		if (position.isPresent()) {
			log.info("existing transaction updated");
			positionRepository.save(position.get());
		}

		AuditDto auditDto = AuditDto.builder().customerId(portfolio.getCustomerId())
				.customerName(portfolio.getCustomerName()).instrumentId(instrument.getInstrumentId())
				.instrumentName(instrument.getInstrumentName()).noOfUnits(transactionDto.noOfUnits())
				.portfolioId(portfolio.getPortfolioId()).tradeType(tradeType.toString())
				.transactionRef(UUID.randomUUID().toString()).build();

		log.info("event created and sending to kafka server");
		producerService.send(auditDto);

		return ApiResponse.builder().httpStatus(SuccessResponse.SUCCESS_CODE2).message(SuccessResponse.SUCCESS_MESSAGE2)
				.build();
	}

}
