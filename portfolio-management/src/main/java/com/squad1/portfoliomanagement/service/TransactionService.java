package com.squad1.portfoliomanagement.service;

import com.squad1.portfoliomanagement.dto.ApiResponse;
import com.squad1.portfoliomanagement.dto.TransactionDto;
import com.squad1.portfoliomanagement.entity.TradeType;

public interface TransactionService {

	ApiResponse transaction(TradeType tradeType, TransactionDto transactionDto);

}
